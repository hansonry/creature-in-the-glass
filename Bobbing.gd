extends Spatial
class_name Bobbing

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _t : float
export(Vector3) var amplitude : Vector3 = Vector3(0, 1, 0)
export(float, 0.00001, 80) var frequency: float = 1
var _base_position : Vector3
var _period: float

# Called when the node enters the scene tree for the first time.
func _ready():
	_t = 0
	_base_position = transform.origin
	_period = 1 / frequency


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var offset = cos(2 * PI * _t * frequency)
	transform.origin = _base_position + (amplitude * offset)
	_t += delta
	_t = fmod(_t, _period)
