extends Area


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _seal :Spatial
var _seal_broken: Spatial
var _rip_sound: AudioStreamPlayer3D
var _outliner : Outliner

export(bool) var ripped :bool = false setget _set_ripped

func _set_ripped(value):
	if ripped != value:
		if value:
			_seal.hide()
			_seal_broken.show()
			_rip_sound.play()
			_outliner.enabled = false
		ripped = value

# Called when the node enters the scene tree for the first time.
func _ready():
	_seal = get_node("seal")
	_seal_broken = get_node("seal_broken")
	_rip_sound = get_node("RipSound")
	_outliner = get_node("Outliner")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Seal_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		self.ripped = true
