extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _cs_nz : CollisionShape
var _cs_pz : CollisionShape
var _cs_nx : CollisionShape
var _cs_px : CollisionShape


# Called when the node enters the scene tree for the first time.
func _ready():
	_cs_nz = get_node("CollisionShapeNZ")
	_cs_pz = get_node("CollisionShapePZ")
	_cs_nx = get_node("CollisionShapeNX")
	_cs_px = get_node("CollisionShapePX")


func disable_walls(camera_angle: float):
	
	camera_angle = rad2deg(camera_angle)
	if camera_angle >= 360:
		camera_angle = fmod(camera_angle, 360)
	elif camera_angle < 0:
		camera_angle = fmod(-camera_angle, 360)
		camera_angle = 360 - camera_angle
	
	#print("Camera Angle: ", camera_angle)
	
	if camera_angle > 270 or camera_angle <= 90:
		_cs_nz.disabled = false
	else:
		_cs_nz.disabled = true
		
		
	if camera_angle > 0 and camera_angle <= 180:
		_cs_nx.disabled = false
	else:
		_cs_nx.disabled = true

	if camera_angle > 90 and camera_angle <= 270:
		_cs_pz.disabled = false
	else:
		_cs_pz.disabled = true

	if camera_angle > 180 or camera_angle <= 0:
		_cs_px.disabled = false
	else:
		_cs_px.disabled = true

	#print("px ", _cs_px.disabled, " nz ", _cs_nz.disabled, " nx ", _cs_nx.disabled, " pz ", _cs_pz.disabled)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
