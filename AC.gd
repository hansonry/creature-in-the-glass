extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


export (int) var _temperature_value : int = 40
export (int) var _temperature_max : int = 95
export (int) var _temperature_min : int = 10
export (int) var _temperature_increment : int = 5
export (float) var _temperature_simulation_rate: float = 0.1

var _simulated_temperature_value: float

# Fan Stuff
var _fan1 : Spatial
var _fan2 : Spatial

var _fan_speed : float
var _target_fan_speed: float

var powered :bool = false

var _label : Label

var _min_sound_db : float = -20
var _max_sound_db : float # Set in GUI

var _min_fan_speed : float = 1
var _max_fan_speed : float = 10

var _air_conditioner_sound : AudioStreamPlayer3D

# Called when the node enters the scene tree for the first time.
func _ready():
	_label = get_node("Viewport/Label")
	_label.text = str(_temperature_value)
	
	_simulated_temperature_value = _temperature_value
	
	_fan1 = get_node("ac_fan")
	_fan2 = get_node("ac_fan2")
	
	_air_conditioner_sound = get_node("AirConditionerSound")
	_max_sound_db = _air_conditioner_sound.unit_db
	_air_conditioner_sound.unit_db = _min_sound_db

func get_temperature_setting():
	return _temperature_value
	
func get_temperature():
	return _simulated_temperature_value

func get_temperature_percent() -> float:
	return float(_simulated_temperature_value - _temperature_min) / float(_temperature_max - _temperature_min)

func _change_temperature(delta: int):
	_temperature_value += delta
	if _temperature_value < _temperature_min:
		_temperature_value = _temperature_min
	if _temperature_value > _temperature_max:
		_temperature_value = _temperature_max
	_label.text = str(_temperature_value)

func _fan_control(delta):
	var diff = abs(_simulated_temperature_value - _temperature_value)
	
	if powered:
		if diff > 10:
			_target_fan_speed = _max_fan_speed
		else:
			_target_fan_speed = _min_fan_speed
	else:
		_target_fan_speed = 0

	var amount = min(1, max(0, delta * 1))
	_fan_speed = lerp(_fan_speed, _target_fan_speed, amount)
	var fan_speed_percent = _fan_speed / _max_fan_speed
	
	if fan_speed_percent > 0:
		if not _air_conditioner_sound.playing:
			_air_conditioner_sound.playing = true
		_air_conditioner_sound.unit_db = lerp(_min_sound_db, _max_sound_db, fan_speed_percent)
	else:
		if _air_conditioner_sound.playing:
			_air_conditioner_sound.playing = false
	
	var rotate_amount = _fan_speed * delta
	_fan1.rotate_z(rotate_amount )
	_fan2.rotate_z(rotate_amount )
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if powered:
		var amount = min(1, max(0, delta * _temperature_simulation_rate))
		_simulated_temperature_value = lerp(_simulated_temperature_value, _temperature_value, amount)
	#print(" Temp: ", _simulated_temperature_value, " Amount: ", amount)
	_fan_control(delta)
		



func _on_ACButtonUp_button_pressed():
	_change_temperature(_temperature_increment)


func _on_ACButtonDown_button_pressed():
	_change_temperature(-_temperature_increment)
