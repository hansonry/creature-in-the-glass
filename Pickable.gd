extends KinematicBody

class_name Pickable

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _ready_called : bool = false
var _put_down_target = null
var _gravity_enabled = true
var velocity : Vector3 = Vector3.ZERO
var _pick_up_target : Vector3 = Vector3.ZERO

var _light :OmniLight
var _light_energy_target: float = 0

var _pickup_sound : AudioStreamPlayer3D
var _drop_sound : AudioStreamPlayer3D

var _aabb : AABB
var _aabb_first : bool
var _obj_radius : float

var _ray_cast : RayCast
var _drop_item_in_frame : int = -1
var _held_item_position : Vector3

var _prev_on_the_floor: bool
export(NodePath) var destination_path : NodePath  setget _destination_path_set
var _destination :PlaceDestination = null

export(bool) var enabled : bool = true setget _enabled_set

export(bool) var in_place : bool = false setget _in_place_set

export (bool) var picked_up : bool = false  setget _picked_up_set

var _outline : Spatial

signal on_placed(me)

func _picked_up_set(value):
	var mod_value = value and enabled and not in_place
	if mod_value != picked_up:
		if mod_value:
			picked_up = true
			_ray_cast.enabled = true
			_held_item_position = global_transform.origin
			_pickup_sound.play()
			input_ray_pickable = false
			_gravity_enabled = false
			collision_layer = 8
			_destination.live = true
		else:
			picked_up = false
			if _outline != null:
				_outline.visible = false
			_ray_cast.enabled = false
			_gravity_enabled = true
			_prev_on_the_floor = false
			input_ray_pickable = true
			collision_layer = 3
			_destination.live = false

func _in_place_set(value):
	assert(in_place == value, "You cannot change this value")


func _enabled_set(value):
	if enabled != value:
		enabled = value
		if _ready_called:
			_ray_cast.enabled = value and picked_up

func _destination_path_set(value):
	if _ready_called:
		var new_dest = get_node(value)
		assert(new_dest != null, "Destination Node doesn't exist: " + value)
		assert(new_dest is PlaceDestination, "Destination node isn't a PlaceDestination: " + value)
		if _destination != null:
			_destination.disconnect("on_placeable_clicked", self, "_on_placeable_clicked")
		_destination = new_dest
		destination_path = value
		_destination.connect("on_placeable_clicked", self, "_on_placeable_clicked")
	else:
		destination_path = value


func _on_placeable_clicked(placeable):
	assert(placeable == _destination, "Placeable is not the destination (How?)")
	_destination.live = false
	in_place = true
	if _outline != null:
		_outline.visible = false
	_drop_item_in_frame = -1
	input_ray_pickable = false
	_gravity_enabled = false
	collision_layer = 8
	_held_item_position = _destination.global_transform.origin
	global_transform.basis = _destination.global_transform.basis
	emit_signal("on_placed", self)


func _collect_aabb(node : Spatial):
	#print(node.name)
	if node is VisualInstance and not node is Light:
		if _aabb_first:
			_aabb = node.get_aabb()
			_aabb_first = false
		else:
			_aabb = _aabb.merge(node.get_aabb())

	elif node is Spatial:
		for child in node.get_children():
			_collect_aabb(child)
	if node is Spatial:
		_aabb = AABB(_aabb.position, _aabb.size * node.transform.basis.get_scale())



# Called when the node enters the scene tree for the first time.
func _ready():
	_ready_called = true
	self.destination_path = destination_path
	_held_item_position = global_transform.origin
	
	_pickup_sound = get_node("PickupSound")
	_drop_sound = get_node("DropSound")
	_light = get_node("OmniLight")
	_ray_cast = get_node("RayCast")
	
	self.enabled = enabled
	
	_light.light_energy = 0
	self.picked_up = false
	_prev_on_the_floor = true
	
	_aabb_first = true
	_collect_aabb(self)
	if _aabb_first:
		_obj_radius = 0
	else:
		_obj_radius = _aabb.get_longest_axis_size()
	#print("OBJ Radius: ", _obj_radius)
	_drop_item_in_frame = -1
	
	# Outline
	_outline = get_node("outline")
	if _outline != null:
		_outline.visible = false

func get_object_radius():
	return _obj_radius

func set_light_on(value: bool):
	if value:
		_light_energy_target = 1
		_light.show()
	else:
		_light_energy_target = 0
		

func _pick_up_to(target : Vector3):
	target.x = max(min(target.x, 2), -2)
	target.z = max(min(target.z, 2), -2)
	_pick_up_target = target

func _physics_process ( delta ):
	if _gravity_enabled:
		velocity.y -= 10 * delta
		move_and_slide(velocity, Vector3.UP)
	elif enabled:
		velocity = Vector3.ZERO
		var diff = _pick_up_target - global_transform.origin
		var vel = diff / (delta * 2)
		if in_place:
			transform.origin += vel * delta
			velocity.y = 0
		else:
			move_and_slide(vel, Vector3.UP)
	var on_floor = is_on_floor()
	if on_floor:
		velocity.y = 0
		if not _prev_on_the_floor:
			_prev_on_the_floor = true
			_drop_sound.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_light.light_energy = lerp(_light.light_energy, _light_energy_target, 0.1)
	if _light.light_energy < 0.001:
		_light.hide()
		
	if in_place:
		_held_item_position = _destination.global_transform.origin
		_pick_up_to(_held_item_position)
	elif picked_up and enabled:
		# Update RayCast to match Camera angle
		var mouse_position = get_viewport().get_mouse_position()
		var camera = get_viewport().get_camera()
		var from = camera.project_ray_origin(mouse_position)
		var dir  = camera.project_ray_normal(mouse_position)
		_ray_cast.global_transform.origin = from
		_ray_cast.global_transform.basis = Basis()
		_ray_cast.cast_to = dir * 1000
		
		if _drop_item_in_frame == 0:
			self.picked_up = false
			_drop_item_in_frame = -1
		else:
			if _ray_cast.is_colliding():
				var point = _ray_cast.get_collision_point()
				var reverse_ray_normal = (from - point).normalized()
				var offset = reverse_ray_normal * get_object_radius()
				_held_item_position = point + offset
				#print("Point ", point, " Offset ", offset)
			_pick_up_to(_held_item_position)
		if _drop_item_in_frame > 0:
			_drop_item_in_frame -= 1


func _unhandled_input(event):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if picked_up:
			# So there appears to be no gaurentee that
			# the colition input events will be trigger before the next frame.
			_drop_item_in_frame = 2

func drop():
	self.picked_up = false

func pickup():
	self.picked_up = true


func _on_Pickable_mouse_entered():
	if enabled and not picked_up:
		set_light_on(1)
		if _outline != null:
			_outline.visible = true

func _on_Pickable_mouse_exited():
	set_light_on(0)
	if _outline != null and not picked_up:
		_outline.visible = false


func _on_Pickable_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		pickup();


