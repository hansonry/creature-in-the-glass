extends StaticBody
class_name Interactable


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(Vector3) var delta_translation: Vector3 = Vector3.ZERO
export(Vector3) var delta_rotation: Vector3 = Vector3.ZERO

var delta_transform: Transform
export(float) var speed : float = 10

export (bool) var state : bool = false setget set_state


export (bool) var lach : bool = false
export (bool) var locked : bool = false
export (bool) var show_locked_wiggle : bool = false

var _original_transform: Transform

var _target_transform: Transform

var _test_value : float
var _target_test_value : float
var _in_motion: bool


var _on_audio     : AudioStreamPlayer3D = null
var _off_audio    : AudioStreamPlayer3D = null
var _close_audio  : AudioStreamPlayer3D = null
var _wiggle_audio : AudioStreamPlayer3D = null

var _outline : Spatial

signal state_changed(state)

func _transform_add( a: Transform,  b: Transform):
	var out: Transform = a
	out.origin += b.origin
	var q1 = out.basis.get_rotation_quat()
	var q2 = b.basis.get_rotation_quat()
	var qf = q1 * q2
	out.basis = Basis(qf)
	return out

func _set_targets():
	if state:
		_target_transform = _transform_add(_original_transform, delta_transform)
		_target_test_value = 1
	else:
		_target_transform = _original_transform
		_target_test_value = 0
	

# Called when the node enters the scene tree for the first time.
func _ready():
	_original_transform = transform
	delta_rotation.x = deg2rad(delta_rotation.x)
	delta_rotation.y = deg2rad(delta_rotation.y)
	delta_rotation.z = deg2rad(delta_rotation.z)
	delta_transform = Transform(Basis(delta_rotation), delta_translation)
	
	set_state(state)
	speed = max(speed, 0)
	
	# Get Sound
	_on_audio = get_node_or_null("OnSound")
	if _on_audio == null:
		_on_audio = get_node_or_null("OpenSound")
	if _on_audio == null:
		_on_audio = get_node_or_null("ActionSound")
	_off_audio = get_node_or_null("OffSound")
	if _off_audio == null:
		_off_audio = get_node_or_null("ActionSound")
	
	_close_audio = get_node_or_null("CloseSound")
	
	_wiggle_audio = get_node_or_null("WiggleSound")

	_outline = get_node("outline")
	if _outline != null:
		_outline.visible = false

func set_state(value: bool):
	state = value
	_set_targets()
	transform = _target_transform

func doit():
	if not locked:
		state = not state
		if state:
			if _on_audio != null:
				_on_audio.play()
		else:
			if _off_audio != null:
				_off_audio.play()
		_set_targets()
		if lach:
			locked = true
		emit_signal("state_changed", state)
	else:
		if _wiggle_audio != null:
			_wiggle_audio.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var amount = min(delta * speed, 1)
	transform = transform.interpolate_with(_target_transform, amount)
	_test_value = lerp(_test_value, _target_test_value, amount)
	
	if abs(_test_value - _target_test_value) < 0.1 :
		if _in_motion and not state and _close_audio != null:
			_close_audio.play()
		_in_motion = false
	else:
		_in_motion = true


func _on_Interactable_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		doit()


func _on_Interactable_mouse_entered():
	if _outline != null and not locked:
		_outline.visible = true


func _on_Interactable_mouse_exited():
	if _outline != null:
		_outline.visible = false
