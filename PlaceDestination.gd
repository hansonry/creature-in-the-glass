extends Area
class_name PlaceDestination


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _animation_player: AnimationPlayer
var live : bool = false setget _live_set

signal on_placeable_clicked(me)

func _live_set(value):
	if live != value:
		live = value
		self.input_ray_pickable = value
		if not live:
			_animation_player.play("LightOn", -1, -1, true)


# Called when the node enters the scene tree for the first time.
func _ready():
	_animation_player = get_node("AnimationPlayer")


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_PlaceDestination_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		if live:
			emit_signal("on_placeable_clicked", self)


func _on_PlaceDestination_mouse_entered():
	if live:
		_animation_player.play("LightOn", -1, 1)


func _on_PlaceDestination_mouse_exited():
	_animation_player.play("LightOn", -1, -1, true)
