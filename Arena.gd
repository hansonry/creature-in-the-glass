extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var camera              : Camera
var _room_static_body   : StaticBody

var held_item           : Spatial
var held_item_position  : Vector3
var _drop_item_in_frame : int 

var _color_filtering : Sprite

var _big_book 

# Special Iteractive Items
var _spin_lock : Spatial
var _ac : Spatial

var _ray_cast :RayCast

export(NodePath) var raw_chicken_path : NodePath
export(NodePath) var cooked_chicken_path: NodePath

# Called when the node enters the scene tree for the first time.
func _ready():
	_ray_cast  = get_node("RayCast")
	camera     = get_node("CameraCenter/CameraRotation/OrbitCamera")
	_room_static_body = get_node("RoomStaticBody")
	held_item  = null
	_drop_item_in_frame = -1
	
	# Special Iteractive Items
	_spin_lock = get_node("CameraCenter/CameraRotation/OrbitCamera/SpinLockUpClose")
	_ac = get_node("AC")
	
	_big_book = get_node("CameraCenter/CameraRotation/OrbitCamera/BigBook")
	
	
	# Shader
	_color_filtering = get_node("ColorFiltering")
	
func place_destination_clicked(place_destination):
	#print("place_destination_clicked ", place_destination)
	if place_destination.place(held_item):
		held_item = null
		_drop_item_in_frame = -1

func _process(delta):
	# Shader Code
	var temp_percent = _ac.get_temperature_percent()
	#print("percent ", temp_percent)
	_color_filtering.material.set_shader_param("temperature", temp_percent)
	
	# Dissable walls in front of the camera
	var camera_angel = camera.get_y_rotation()
	_room_static_body.disable_walls(camera_angel)

func _on_StaticBody_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_spin_lock.player_look_at()


func _on_TestLockArea_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_spin_lock.player_look_at()


func _on_Microwave_chicken_cooked():
	var chicken_raw    = get_node(raw_chicken_path)
	var chicken_cooked = get_node(cooked_chicken_path)
	
	chicken_raw.enabled = false
	chicken_raw.visible = false
	chicken_cooked.enabled = true
	chicken_cooked.visible = true


func _on_book_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_big_book.show_big_book()
