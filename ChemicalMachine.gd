extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


var _chem_mach_lever0 : Interactable
var _chem_mach_lever1 : Interactable
var _chem_mach_lever2 : Interactable
var _chem_mach_lever3 : Interactable

export (bool) var powered : bool = false setget set_powered 
export (int, 0, 15) var lever_state : int = 15 setget set_lever_state

var _data : Array

signal change(value)

# Called when the node enters the scene tree for the first time.
func _ready():
	_chem_mach_lever0 = get_node("ChemicalMachineLever0/Interactable")
	_chem_mach_lever1 = get_node("ChemicalMachineLever1/Interactable")
	_chem_mach_lever2 = get_node("ChemicalMachineLever2/Interactable")
	_chem_mach_lever3 = get_node("ChemicalMachineLever3/Interactable")
	
	_chem_mach_lever0.connect("state_changed", self, "_lever_was_pulled")
	_chem_mach_lever1.connect("state_changed", self, "_lever_was_pulled")
	_chem_mach_lever2.connect("state_changed", self, "_lever_was_pulled")
	_chem_mach_lever3.connect("state_changed", self, "_lever_was_pulled")

	var red    = _chem_mach_lever3
	var green  = _chem_mach_lever2
	var blue   = _chem_mach_lever1
	var yellow = _chem_mach_lever0

	_data = [
		{ 'light': get_node("LightBoard/Light0"),  'levers': [red, yellow, green] },
		{ 'light': get_node("LightBoard/Light1"),  'levers': [red, blue] },
		{ 'light': get_node("LightBoard/Light2"),  'levers': [yellow, blue] },
		{ 'light': get_node("LightBoard/Light3"),  'levers': [red, blue] },
		{ 'light': get_node("LightBoard/Light4"),  'levers': [yellow] },
		{ 'light': get_node("LightBoard/Light5"),  'levers': [red] },
		{ 'light': get_node("LightBoard/Light6"),  'levers': [blue, green] },
		{ 'light': get_node("LightBoard/Light7"),  'levers': [red, green] },
		{ 'light': get_node("LightBoard/Light8"),  'levers': [red, green] },
		{ 'light': get_node("LightBoard/Light9"),  'levers': [green] },
		{ 'light': get_node("LightBoard/Light10"), 'levers': [green, yellow] },
		{ 'light': get_node("LightBoard/Light11"), 'levers': [red, yellow] },
		{ 'light': get_node("LightBoard/Light12"), 'levers': [red, blue, green] },
		{ 'light': get_node("LightBoard/Light13"), 'levers': [blue, yellow] },
		{ 'light': get_node("LightBoard/Light14"), 'levers': [blue, green, yellow] },
		{ 'light': get_node("LightBoard/Light15"), 'levers': [blue, yellow] },
	]
	set_lever_state(lever_state)
	
	_update_lights()

func _compute_lever_state():
	var state : int = 0
	if _chem_mach_lever0.state:
		state |= 0x1
	if _chem_mach_lever1.state:
		state |= 0x2
	if _chem_mach_lever2.state:
		state |= 0x4
	if _chem_mach_lever3.state:
		state |= 0x8
	return state

func set_lever_state(value: int):
	lever_state = value
	_chem_mach_lever0.state = (value & 0x1) == 0x1
	_chem_mach_lever1.state = (value & 0x2) == 0x2
	_chem_mach_lever2.state = (value & 0x4) == 0x4
	_chem_mach_lever3.state = (value & 0x8) == 0x8
	

func _lever_was_pulled(state):
	_update_lights()
	lever_state = _compute_lever_state()
	emit_signal("change", lever_state)

func _update_lights():
	for light_data in _data:
		var light = light_data['light']
		var levers = light_data['levers']
		# Count up active levers
		var lever_count : int = 0
		for lever in levers:
			if lever.state:
				lever_count += 1
		# Turn light on if active lever count is even
		if (lever_count % 2) == 0:
			light.show()
		else:
			light.hide()
		
	
func set_powered(new_value :bool):
	powered = new_value
	if powered:
		_chem_mach_lever0.locked = false
		_chem_mach_lever1.locked = false
		_chem_mach_lever2.locked = false
		_chem_mach_lever3.locked = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
