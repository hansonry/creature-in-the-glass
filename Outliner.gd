extends Spatial

class_name Outliner

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(bool) var enabled : bool = true setget _set_enabled

func _set_enabled(value):
	if enabled != value:
		if not value and visible:
			visible = false
		enabled = value


# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false

func on_mouse_entered():
	if enabled:
		visible = true

func on_mouse_exited():
	visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

