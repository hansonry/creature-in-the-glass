extends MeshInstance


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var mat = self.get_surface_material(0)
	mat.uv1_offset.x += .02
