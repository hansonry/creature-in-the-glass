extends Spatial
class_name SpinLockWheel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _value: int = 6
var _target_angle: float
var _current_angle: float
var _original_basis: Basis
var _wheel_mesh : Spatial
var _value_changed : bool

var _wheel_spin_sound : AudioStreamPlayer3D

export (float) var wheel_speed: float = 10 
	
signal new_value(value)

# Called when the node enters the scene tree for the first time.
func _ready():
	_wheel_spin_sound = get_node("WheelSpinSound")
	_wheel_mesh = get_node("MeshInstance")
	_original_basis = _wheel_mesh.transform.basis
	_target_angle = 0
	_current_angle = _target_angle
	_update_angle()
	_value_changed = false

func _update_angle():
	_wheel_mesh.transform.basis = _original_basis.rotated(Vector3.UP, _current_angle)
	

func get_value():
	return _value

func spin(delta: int):
	#print("delta: ", delta)
	var to_add = delta
	if to_add < 0:
		to_add = 10 + to_add
	_value = (_value + to_add) % 10
	_target_angle -= delta * TAU / 10
	#print("value: ", _value)
	if delta != 0:
		_value_changed = true
		_wheel_spin_sound.play()

func _on_CountUp_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		spin(-1)


func _on_CountDown_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		spin(1)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var amount = min(1, wheel_speed * delta)
	_current_angle = lerp(_current_angle, _target_angle, amount)
	_update_angle()
	if _value_changed and abs(_target_angle - _current_angle) < 0.01:
		_value_changed = false
		emit_signal("new_value", _value) 
