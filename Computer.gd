extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (bool) var system_ready : bool = false setget set_system_ready
export (float) var error_timeout : float = 0.75


var _screen_atlas : AtlasTexture

var _error_timer: float

var _fail_sound : AudioStreamPlayer3D
var _boot_sound : AudioStreamPlayer3D

signal computer_success()

# Called when the node enters the scene tree for the first time.
func _ready():
	_screen_atlas = get_node("Sprite3D").texture
	_fail_sound   = get_node("FailSound")
	_boot_sound   = get_node("BootSound")
	_error_timer  = 0

func set_system_ready(value: bool):
	if system_ready != value:
		system_ready = value
		if system_ready:
			set_screen_o()
			_boot_sound.play()
			_error_timer = 0
		else:
			set_screen_black()

func set_screen_black():
	_screen_atlas.region.position = Vector2(19, 13)

func set_screen_x():
	_screen_atlas.region.position = Vector2(0, 0)

func set_screen_o():
	_screen_atlas.region.position = Vector2(19, 0)

func set_screen_check():
	_screen_atlas.region.position = Vector2(0, 13)


func _show_error():
	_error_timer = error_timeout
	set_screen_x()
	_fail_sound.play()

func _show_success():
	_error_timer = error_timeout
	set_screen_check()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if _error_timer > 0:
		if _error_timer <= delta:
			_error_timer = 0
			if system_ready:
				set_screen_o()
			else:
				set_screen_black()
		else:
			_error_timer -= delta

func _computer_activated():
	if system_ready:
		_show_success()
		emit_signal("computer_success")
		system_ready = false
	else:
		_show_error()

func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_computer_activated()

