extends Spatial

export(String) var unlock_code : String = "123"

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var _code_wheels : Array
var _shackle : Spatial
var _shackle_target_y : float
var _is_locked: bool


var _original_y : float
var _target_y : float

var _unlock_sound : AudioStreamPlayer3D
# Called when the node enters the scene tree for the first time.
func _ready():
	_unlock_sound = get_node("UnlockSound")
	_shackle = get_node("Spatial/lock_arm")
	_code_wheels = [
		get_node("Spatial/Wheel1"),
		get_node("Spatial/Wheel2"),
		get_node("Spatial/Wheel3"),
	]
	
	_is_locked = true
	# Attach the wheels
	for wheel in _code_wheels:
		wheel.connect("new_value", self, "_on_wheel_new_value")
		
	_shackle_target_y = _shackle.transform.origin.y
	_original_y = transform.origin.y
	transform.origin.y += 5
	_target_y = transform.origin.y

func _get_current_combination():
	var code = ""
	for wheel in _code_wheels:
		code = code + str(wheel.get_value())
	return code
	
func is_valid_code():
	var code = _get_current_combination()
	return code == unlock_code
	
func _on_wheel_new_value(value):
	print(_get_current_combination())
	if _is_locked and is_valid_code():
		_shackle_target_y += 0.5
		_is_locked = false
		_unlock_sound.play()

func _smooth_lerp(start, target, speed, delta):
	var amount = min(1, delta * speed)
	return lerp(start, target, amount)
	

func player_look_at():
	show()
	_target_y = _original_y

func player_look_away():
	_target_y += 5

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_shackle.transform.origin.y = _smooth_lerp(_shackle.transform.origin.y, _shackle_target_y, 10, delta)
	transform.origin.y  = _smooth_lerp(transform.origin.y, _target_y, 10, delta)



func _on_ClickAway_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		player_look_away()
