extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Overall
var _sm: StateMachine
var _animation1 : AnimationPlayer
var _animation2 : AnimationPlayer
var _animation3 : AnimationPlayer

onready var _big_book: BigBook = get_node("../CameraCenter/CameraRotation/OrbitCamera/BigBook")

# Phase1
var _generator_lever : Interactable
var _generator_sound : AudioStreamPlayer3D
var _lights : Spatial
var _ac : Spatial

## Sounds
var _background_music : AudioStreamPlayer
var _emergency_light_loop : AudioStreamPlayer

# Phase2
var _microwave : Spatial
var _light_switch : Spatial
var _pap_pipe_segment_1 : Pickable
var _pap_pipe_segment_2 : Pickable
var _pap_pipe_segment_3 : Pickable
var _chem_machine : Spatial
var _computer : Spatial

# Phase3
# Stuff already loaded

# Phase4
var _bubbles : CPUParticles
var _light_flicker_sound : AudioStreamPlayer3D
var _egg_crack_sound : AudioStreamPlayer3D

var _baby_sprite : AnimatedSprite3D

var _raw_chicken : Pickable

var _outro

# Called when the node enters the scene tree for the first time.
func _ready():
	# Overall
	_animation1 = get_node("AnimationPlayer1")
	_animation2 = get_node("AnimationPlayer2")
	_animation3 = get_node("AnimationPlayer3")
	_sm = get_node("StateMachine")
	
	# Phase1
	_generator_lever = get_node("generator_lever")
	_generator_sound = get_node("generator/AudioStreamPlayer3D")
	_lights = get_node("../Lights")
	_ac = get_node("../AC")
	
	## Sounds
	_background_music = get_node("../BackgroundMusic")
	_emergency_light_loop = get_node("../EmergencyLightLoop")
	
	# Phase2
	_light_switch = get_node("LightSwitch") 
	_pap_pipe_segment_1 = get_node("pick_pipe_segment1")
	_pap_pipe_segment_2 = get_node("pick_pipe_segment2")
	_pap_pipe_segment_3 = get_node("pick_pipe_segment3")
	_chem_machine = get_node("ChemicalMachine")
	_computer = get_node("Computer")
	_microwave = get_node("Microwave")
	
	# Phase3
	# Stuff already loaded
	
	
	# Phase4
	_bubbles = get_node("../Bubbles")
	_light_flicker_sound = get_node("LightFlickerSound")
	_egg_crack_sound = get_node("../Bobbing/BabySprite/EggHatchSound")
	_baby_sprite = get_node("../Bobbing/BabySprite")
	_raw_chicken = get_node("PAP_chicken_raw")
	
	
	_outro = get_node("../CameraCenter/CameraRotation/OrbitCamera/Outro")
	
	# State Machine
	_sm.set_object(self)
	_sm.changed_state("_state_intro")
	
func book_unlock_and_set_chapter(chapter):
	_big_book.unlock_chapter(chapter)
	_big_book.set_chapter(chapter)

func _state_intro(action:String, data):
	if action == 'enter':
		_sm.changed_state("_state_phase1")
	

func _state_phase1(action:String, data):
	if action == 'enter':
		_lights.set_emergency()
		_ac.powered = false
	elif action == 'process':
		var delta = data
	elif action == 'generator_gas_can_in_place':
		_animation1.play('fill_generator_gas')
		_generator_lever.locked = false
	elif action == 'generator_lever_state_changed':
		_sm.changed_state("_state_phase2")

func _update_lights():
	if _light_switch.is_on():
		_lights.set_on()
	elif _light_switch.is_dim():
		_lights.set_dim()
	elif _light_switch.is_off():
		_lights.set_off()

func _state_phase2(action:String, data):
	if action == 'enter':
		_emergency_light_loop.stop()
		_ac.powered = true
		_generator_sound.play()
		_background_music.play()
		_update_lights()
		_pap_pipe_segment_1.enabled = true
		_pap_pipe_segment_2.enabled = true
		_pap_pipe_segment_3.enabled = true
		_chem_machine.powered = true
		_microwave.powered = true
		_animation2.play("move_camera")
	elif action == 'light_switch':
		_update_lights() 
	elif action == 'process':
		_computer.system_ready = (_chem_machine.lever_state == 4 and
								  _pap_pipe_segment_1.in_place and
								  _pap_pipe_segment_2.in_place and
								  _pap_pipe_segment_3.in_place)
	elif action == 'computer_success':
		_sm.changed_state("_state_phase3")
		
func _state_phase3(action:String, data):
	if action == 'enter':
		_animation1.play("container_fill")
		book_unlock_and_set_chapter(2)
	elif action == 'light_switch':
		_update_lights()
	elif action == 'process':
		_computer.system_ready = (_light_switch.is_dim() and 
								  _ac.get_temperature() > 90)
	elif action == 'computer_success':
		_sm.changed_state("_state_phase4")

func _state_phase4(action:String, data):
	if action == 'enter':
		_bubbles.emitting = true
		_light_flicker_sound.play()
		_egg_crack_sound.play()
		_baby_sprite.play()
		book_unlock_and_set_chapter(3)
	elif action == 'light_switch':
		_update_lights()
	elif action == 'process':
		if (_chem_machine.lever_state == 12 and
			_ac.get_temperature() < 30):
			_sm.changed_state("_state_ending")


func _state_phase5(action:String, data):
	if action == 'enter':
		print("Phase5")
		pass
		
func _state_ending(action:String, data):
	if action == 'enter':
		print("Ending")
		_animation3.play("ending")
	elif action == 'light_switch':
		_update_lights()

func play_outro():
	_outro.show()
	_outro.get_node("AnimationPlayer").play("Bad End")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	_sm.call_state('process', delta)

func _on_generator_lever_state_changed(state):
	_sm.call_state('generator_lever_state_changed', state)

func _on_LightSwitch_switch(position):
	_sm.call_state('light_switch', position)

func _on_ChemicalMachine_change(value):
	_sm.call_state('chemical_machine_change', value)

func _on_Computer_computer_success():
	_sm.call_state('computer_success')


func _on_pick_generator_gas_can_on_placed(me):
	_sm.call_state('generator_gas_can_in_place')
