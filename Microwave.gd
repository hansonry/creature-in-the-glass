extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (bool) var raw_chicken_inside : bool = false setget set_raw_chicken_inside
export (bool) var powered            : bool = false setget set_powered
export (bool) var cooked_chicken     : bool = false 

var _animation: AnimationPlayer
var _running_sound: AudioStreamPlayer3D

var _door : Interactable

signal chicken_cooked()

# Called when the node enters the scene tree for the first time.
func _ready():
	_animation = get_node("AnimationPlayer")
	_running_sound = get_node("RunningSound")
	_door = get_node("door")


func _start_cook_timer():
	_door.locked = true
	_animation.play('cook')

func set_raw_chicken_inside(value: bool):
	if value != raw_chicken_inside:
		raw_chicken_inside = value
			
func set_powered(value: bool):
	if value != powered:
		powered = value

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Interactable_state_changed(state):
	if not state and powered and raw_chicken_inside and not cooked_chicken:
		_start_cook_timer()
	elif state:
		_running_sound.stop()


func unlock_microwave():
	_door.locked = false
	cooked_chicken = true
	emit_signal("chicken_cooked")

func open_microwave_door_if_closed():
	if not _door.state:
		_door.doit()



func _on_pick_chicken_raw_on_placed(me):
	raw_chicken_inside = true
