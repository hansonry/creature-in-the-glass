extends Spatial


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _light_kitchen : OmniLight
var _light_emergency : OmniLight
var _light_desk : OmniLight
var _light_chems : OmniLight


# Called when the node enters the scene tree for the first time.
func _ready():
	_light_kitchen   = get_node("Light_Kitchen")
	_light_emergency = get_node("Light_Emergency")
	_light_desk      = get_node("Light_Desk")
	_light_chems     = get_node("Light_Chems")

func set_emergency():
	_light_kitchen.light_energy   = 0
	_light_emergency.light_energy = 1.3
	_light_desk.light_energy      = 0
	_light_chems.light_energy     = 0

func set_off():
	_light_kitchen.light_energy   = 0.5
	_light_emergency.light_energy = 0.5
	_light_desk.light_energy      = 0.5
	_light_chems.light_energy     = 0.5
	
func set_dim():
	_light_kitchen.light_energy   = 0.9
	_light_emergency.light_energy = 0.9
	_light_desk.light_energy      = 0.9
	_light_chems.light_energy     = 0.9
	
func set_on():
	_light_kitchen.light_energy   = 1.3
	_light_emergency.light_energy = 1.3
	_light_desk.light_energy      = 1.3
	_light_chems.light_energy     = 1.3
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
