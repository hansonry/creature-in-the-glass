extends Area


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _animation : AnimationPlayer
var _sprite: AnimatedSprite3D

var is_drawn: bool

# Called when the node enters the scene tree for the first time.
func _ready():
	_animation = get_node("AnimationPlayer")
	_sprite = get_node("AnimatedSprite3D")
	is_drawn = false
	_sprite.transform.origin.y = -0.03


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _next_action():
	if is_drawn:
		is_drawn = false
		_animation.play("erase")
	else:
		is_drawn = true
		var last_frame = _sprite.frame 
		_sprite.frame +=1
		if last_frame == _sprite.frame:
			_sprite.frame = 0
		_animation.play("draw")

func _on_Glyph_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_next_action()
