extends StaticBody


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export (float) var switch_y_off : float = -0.025
export (float) var switch_y_dim : float =  0
export (float) var switch_y_on  : float =  0.025
export (float) var switch_speed : float =  20
export (String, 'on', 'dim', 'off') var switch_position: String = 'on'

signal switch(position)

var _switch           : Spatial
var _switch_sound     : AudioStreamPlayer3D
var _switch_off_sound : AudioStreamPlayer3D


var _target_y : float

# Called when the node enters the scene tree for the first time.
func _ready():
	_switch           = get_node("lightswitch_switch")
	_switch_sound     = get_node("SwitchSound")
	_switch_off_sound = get_node("SwitchOffSound")
	
	_target_y = _get_switch_position(switch_position)
	_switch.transform.origin.y = _target_y
	

func is_on()  -> bool:
	return switch_position == 'on'
func is_dim() -> bool:
	return switch_position == 'dim'
func is_off() -> bool:
	return switch_position == 'off'


func _get_switch_position(pos : String) -> float:
	var y : float
	if   is_on():
		y = switch_y_on
	elif is_dim():
		y = switch_y_dim
	elif is_off():
		y = switch_y_off
	else:
		assert(false, "Possition of " + pos + " not supported")
	return y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var amount = delta * switch_speed
	_switch.transform.origin.y = lerp(_switch.transform.origin.y, _target_y, amount)


func _switch_clicked():
	if   is_on():
		switch_position = 'off'
	elif is_dim():
		switch_position = 'on'
	elif is_off():
		switch_position = 'dim'
	_target_y = _get_switch_position(switch_position)
	emit_signal("switch", switch_position)
	if is_on() or is_dim():
		_switch_sound.play()
	else: 
		_switch_off_sound.play()


func _on_Area_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_switch_clicked()

