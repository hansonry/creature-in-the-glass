extends Area

class_name ACButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal button_pressed()

var _button_pressed_sound : AudioStreamPlayer3D

var _outline: Spatial


# Called when the node enters the scene tree for the first time.
func _ready():
	_button_pressed_sound = get_node("ButtonPressedSound")
	_outline = get_node("outline")
	_outline_hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _outline_show():
	if _outline != null:
		_outline.visible = true
	
func _outline_hide():
	if _outline != null:
		_outline.visible = false
	

func _on_ACButton_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_button_pressed_sound.play()
		emit_signal("button_pressed")


func _on_ACButton_mouse_entered():
	_outline_show()


func _on_ACButton_mouse_exited():
	_outline_hide()
