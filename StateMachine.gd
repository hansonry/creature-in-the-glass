extends Node
class_name StateMachine

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _func_ref : FuncRef
var _target_state: String
var _target_reenter: bool

var _valid_state: bool
var _change_state_depth: int

# Called when the node enters the scene tree for the first time.
func _ready():
	_valid_state = false
	_func_ref = FuncRef.new()
	_target_state = ''
	_target_reenter = false
	_change_state_depth = 0

func set_object(obj):
	_func_ref.set_instance(obj)

func _needs_to_transition(reenter: bool):
	return ((not _valid_state and _target_state != '') or
			(_valid_state and _target_state == '') or
			_target_state != _func_ref.function or
			reenter and _target_state != '')

func _move_states():
	_change_state_depth += 1
	while _needs_to_transition(_target_reenter):
		if _valid_state:
			call_state("exit")
			_valid_state = false
			_target_reenter = false
		else:
			_func_ref.function = _target_state
			_valid_state = _func_ref.is_valid()
			assert(_valid_state, "ERROR: " + _target_state + " is not a valid function");
			call_state("enter")
	_change_state_depth -= 1

func changed_state(state_name: String, reenter: bool = false):
	_target_state = state_name
	_target_reenter = reenter
	if _change_state_depth <= 0:
		_move_states()
		


func reenter():
	changed_state(_func_ref.function, true)

func call_state(command : String, data = null):
	_func_ref.call_func(command, data)

func get_current_state() -> String:
	return _func_ref.function

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
