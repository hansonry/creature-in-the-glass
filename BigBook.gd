extends Area

class_name BigBook

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var _animation : AnimationPlayer
var _sprite : AnimatedSprite3D
var _page_turn_sound : AudioStreamPlayer3D

var _min_frame: int
var _max_frame: int

onready var _page_right_dogear : Outliner = get_node("PageRight/Outliner")
onready var _page_left_dogear  : Outliner = get_node("PageLeft/Outliner")

# Called when the node enters the scene tree for the first time.
func _ready():
	_sprite = get_node("AnimatedSprite3D")
	_animation = get_node("AnimationPlayer")
	_page_turn_sound = get_node("PageTurnSound")
	
	_min_frame = 0
	unlock_chapter(1)
	_update_dogears()

func _update_dogears():
	_page_left_dogear.enabled = not (_sprite.frame == _min_frame)
	_page_right_dogear.enabled = not (_sprite.frame == (_max_frame - 1))

func unlock_chapter(chapter):
	_max_frame = chapter + 1
	

func set_frame(frame):
	_sprite.frame = frame
	_update_dogears()


func set_chapter(chapter):
	set_frame(chapter)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func show_big_book():
	_animation.play('pop_up')

func page_next():
	var frame = _sprite.frame + 1
	if frame < _max_frame:
		set_frame(frame)
		_page_turn_sound.play()

func page_previous():
	var frame = _sprite.frame - 1
	if frame >= _min_frame:
		set_frame(frame)
		_page_turn_sound.play()

func _on_PageRight_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		page_next()


func _on_PageLeft_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		page_previous()


func _on_Click_Away_input_event(camera, event, click_position, click_normal, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT:
		_animation.play('pop_out')
